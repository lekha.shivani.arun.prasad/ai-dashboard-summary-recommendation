from flask import Flask, request, g, current_app
from flask import Flask, jsonify, request, send_file, render_template, redirect
from flask_cors import CORS, cross_origin
from langchain.agents import create_csv_agent
from langchain.agents.agent_types import AgentType
from langchain.agents.tools import InvalidTool
from langchain.tools import BaseTool
import langchain.agents.agent
from langchain.llms import AzureOpenAI
from langchain.agents import ZeroShotAgent, Tool, AgentExecutor
from langchain.memory import ConversationBufferMemory, ReadOnlySharedMemory
from langchain import OpenAI, LLMChain, PromptTemplate
import langchain
import langchain.agents.loading
import openai
import os



# API_KEY="f93979cbf9894257affd4fee8b4e08fb"
# API_BASE="https://sravanakumar13sathish.openai.azure.com/"
# COMPLETION_MODEL = "Policy_GPT"
# EMBEDDING_MODEL="text-embedding-ada-002"
# API_VERSION='2023-03-15-preview'
# API_TYPE="azure"

API_KEY = "d5cd9e479fb84ef698dead338e5b8a9b"
API_BASE = "https://jayasai01.openai.azure.com/"
API_VERSION = '2023-03-15-preview'
EMBEDDING_MODEL="text-embedding-ada-002"
API_TYPE = "azure"
DEPLOYMENT_NAME = "GPT_3_5"


# For text-ada-001

# DEPLOYMENT_NAME=TRIAL_PURPOSE
# API_VERSION=2022-12-01

# For text-davinci-003

# DEPLOYMENT_NAME=DEMO_PURPOSE
# API_VERSION=2022-12-01

# openai.api_key = API_KEY
# openai.api_base = API_BASE
# openai.api_type=API_TYPE
# openai.api_version=API_VERSION

os.environ["OPENAI_API_TYPE"] = API_TYPE
os.environ["OPENAI_API_BASE"] = API_BASE
os.environ["OPENAI_API_KEY"] = API_KEY
os.environ["OPENAI_API_VERSION"] = API_VERSION



llm = AzureOpenAI(
    openai_api_type="azure",
    deployment_name=DEPLOYMENT_NAME, 
    model_name="text-ada-001")

# Create the Flask app
app = Flask(__name__)
# run_with_ngrok(app)

# Define the global variable to store the agent
AGENT_FILE_PATH = 'agent.yaml'


@app.route('/', methods = ['GET', 'POST'])
@cross_origin()
def home():
    if(request.method == 'GET'):
        resp = jsonify({'message' : 'rendered successfully'})
        return resp


# Route for uploading the file and creating the agent
@app.route('/upload', methods=['POST'])
@cross_origin(supports_credentials=True)
def upload_file():
    global response
    try:
        # data = request.json
        # Get the uploaded file
        file = request.files['file']
        question = request.form['question']
        # Save the file to a location
        file.save('uploaded_file.csv')

        # Create the agent using the uploaded file
        agent = create_csv_agent(llm, 'uploaded_file.csv', verbose=True)

        # Store the agent in the global variable
        agent.save_agent(AGENT_FILE_PATH)
        
        message = """For the following query, if it requires drawing a table, reply as follows:\n{table: {columns: [column1, column2, ...], data: [[value1, value2, ...], [value1, value2, ...], ...]}}\n\nIf the query requires creating a bar chart, reply as follows:\njavascriptFunction:"data => { let result = {}; data.forEach(row => { if(row['origin_1']) { if(result[row['origin_1']]) { result[row['origin_1']] += 1; } else { result[row['origin_1']] = 1; } } }); let values = []; for(let key in result) { if(result.hasOwnProperty(key)) { values.push({ x: key, y: result[key] }); } } return values.slice(0, 5); }"\n\nIf the query requires creating a line chart, reply as follows:\n{"line": {"columns": ["A", "B", "C", ...], "data": [25, 24, 10, ...]}}\n\nThere can only be two types of chart, "bar" and "line".\n\nIf it is just asking a question that requires neither, reply as follows:\n\n{"answer": "answer"}\nExample:\n\n{"answer": "The title with the highest rating is 'Gilead'"}\n\nIf you do not know the answer, reply as follows:\n{"answer": "I do not know."}\n\nReturn all output as a string.\n\nAll strings in "columns" list and data list, should be in double quotes,\n\nFor example: {"columns": ["title", "ratings_count"], "data": [["Gilead", 361], ["Spider's Web", 5164]]}\n\nLets think step by step."""
        prompt = [{"role":"system","content":message},
                {"role":"user","content":question}]
        # Run the prompt through the agent and retrieve the output tokens.
        response = agent.run(prompt)
        print(response)
        # return jsonify({"answer":response})
        #return 'Hello'
        return redirect('/response')
    except Exception as e:
        #return jsonify({"error":e})
        return str(e)

@app.route('/response', methods=['GET'])
@cross_origin(supports_credentials=True)
def show_ans():
    global response
    if response is None:
        return 'No response'
    return response


# Run the Flask app
if __name__ == '__main__':
    CORS(app, support_credentials=True)
    # app.run()
    app.run(debug = True,host='0.0.0.0',port=5000)








